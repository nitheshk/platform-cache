- - -
#Platform Cache Features
- - - 

Platform Cache is a memory layer that stores Salesforce session and org data for later access. When you use Platform Cache, your applications can run faster because they store reusable data in memory. Applications can quickly access this data; they don’t need to duplicate calculations and requests to the database on subsequent transactions. In short, think of Platform Cache as RAM for your cloud application.

The Platform Cache API lets you store and retrieve data that’s tied to Salesforce sessions or shared across your org. Put, retrieve, or remove cache values by using the Session, Org, SessionPartition, and OrgPartition classes in the Cache namespace. Use the Platform Cache Partition tool in Setup to create or remove org partitions and allocate their cache capacities to balance performance across apps.

There are two types of cache:

- **Session cache** : Stores data for individual user sessions. For example, in an app that finds customers within specified territories, the calculations that run while users browse different locations on a map are reused.
    Session cache lives alongside a user session. The maximum life of a session is eight hours. Session cache expires when its specified time-to-live (ttlsecs value) is reached or when the session expires after eight hours, whichever comes first.
- **Org cache** : Stores data that any user in an org reuses. For example, the contents of navigation bars that dynamically display menu items based on user profile are reused.
    Unlike session cache, org cache is accessible across sessions, requests, and org users and profiles. Org cache expires when its specified time-to-live (ttlsecs value) is reached.

### The best data to cache is:

- Reused throughout a session
- Static (not rapidly changing)
- Otherwise expensive to retrieve
- - -

### Before We Go Any Further

- Let’s pause for a moment for you to request a **trial of Platform Cache**. By default, your Developer org has 0 MB cache capacity. You can request a trial cache of 10 MB.
- To request a trial,
    - Go to **Setup** in your Developer org. 
    - In the Quick Find box, enter cache, and then click **Platform Cache**. 
    - Click **Request Trial Capacity** and wait for the email notifying you that your Platform Cache trial is active. Salesforce approves trial requests immediately, but it can take a few minutes for you to receive the email.

 
Before you can use cache space in your org, you must create partitions to define how much capacity you want for your apps. Each partition capacity is broken down between **org cache** and **session cache**. Session and org cache allocations can be zero, 5 MB, or greater, and must be whole numbers. **The minimum size of a partition, including its org and session cache allocations, is 5 MB**. For example, say that your org has a total of 10-MB cache space and you created a partition with a total of 5 MB, 5 MB of which is for session cache and 0 MB for org cache. Or you can create a partition of 10-MB space, with 5 MB for org cache and 5 MB for session cache. The sum of all partitions, including the default partition, equals the Platform Cache total allocation.

### To get started, let’s create one partition from the Platform Cache page 

- In Setup, enter Platform Cache in the Quick Find box, then select Platform Cache.
- Click New Platform Cache Partition.
- Give the partition a name (such as the name of your application).
- Check Default Partition.
- Enter 5 for session cache and 5 for org cache, and then click Save.
- - -
 	
### Default Partition

You can define any partition as the default partition, but you can have only **one default partition**. The default partition enables you to use shorthand syntax to perform cache operations on that partition. This means that you don’t have to fully qualify the key name with the namespace and partition name when adding a key-value pair. For example, instead of calling **Cache.Org.put('namespace.partition.key', 0); you can just call Cache.Org.put('key', 0);**
Unlike with Apex methods, you can’t omit the namespace.partition prefix to reference the default partition in the org. If a namespace isn’t defined for the org, use **local** to refer to the namespace of the current org where the code is running.
Cache.Org.put(‘local.partition.key', 0); 

> If **no default partition** is specified in the org, calling a cache method without fully qualifying the key name causes a **Cache.Session.SessionCacheException** to be thrown.
- - -

##How to use Session cache and Org Cache

> Example for Session Cache 

```
public class SessionCacheController {
    public  static String name {get;set;}
    public SessionCacheController(){
    name='';
    }
    public static void setData()   {      
       // Cache.Session.put('myName', 'Nithesh K'); //  if Default Partition is enabled 
       Cache.Session.put('MyPackagePrefix.PlatformCache.myName', 'Nithesh K');   
    }
   
    public static void getData()   {
       // Name=(String)Cache.Session.get('myName'); //  if Default Partition is enabled
    if (Cache.Session.contains(‘myName'))
    	 Name=(String)Cache.Session.get('MyPackagePrefix.PlatformCache.myName');   
    }
}
```
[More Details on session Cache](https://developer.salesforce.com/docs/atlas.en-us.apexcode.meta/apexcode/apex_class_cache_Session.htm#apex_class_cache_Session)

> Example for Org Cache 

```
public class orgCacheController {
    public  static String name {get;set;}
    public SessionCacheController(){
    name='';
    }
    public static void  setData()    {      
    // Cache.Org.put('myName', 'Nithesh K');   //  if Default Partition is enabled
      Cache.Org.put('MyPackagePrefix.PlatformCache.myName', 'Nithesh K'); 
    }
    
    public static void getData()    {     
    // Name=(String)Cache.Org.get('myName');   ); //   if Default Partition is enabled
     if (Cache. Org.contains(‘myName’))
        Name=(String)Cache.Org.get('MyPackagePrefix.PlatformCache.myName’);   
    }
}
```

> [More Details on Org Cache](https://developer.salesforce.com/docs/atlas.en-us.apexcode.meta/apexcode/apex_class_cache_Org.htm#apex_class_cache_Org)
- - -

 Best example when use with RemoteAction Methods, RemoteAction methods are static, and can't store Values. In remote action each transaction in Sales-force starts as a fresh slate, with nothing loaded in memory.

```
Global class PolicyHolderDetails {
    public class Member {
        Public string Name{get;set;}
        // declare your Member here 
        Public Member(){}
    }

    @RemoteAction global static Member Save(String Name){
    // some stuff.
    Member member=new Member();
    member.Name= Name;
    return Member; // Store the Member in visual Force page.
    }

   // Resend member again from visual force Page to your Second remote action
    @RemoteAction global static String retrieveMemberName (Member member) {
      // Use Member here     
        Return Member.Name;
    }
}
```

Above example define how to remoting access paramter from one method into other, without cache.

Another Alternative way is using cache,keep member in server Cache. Way of doing this would be to use the Platform Cache to store the data in server side. You can use a Session Cache to store the required data between Javascript Remoting calls and get it from cache when you need it.

```
Global class PolicyHolderDetails {
 public class Member {
     Public string Name{get;set;}
     // declare your Member here 
     Public Member(){}
     }
     @RemoteAction global static void  Save(String Name){
     // some stuff.
     Member member=new Member();
     member.Name= Name;
     // Add Member to the cache.
     Cache.Session.put('member', member);
     }

     @RemoteAction global static String retrieveMemberName(){
     // Return cached value whose type is the inner class Member.
    Member member= (Member)Cache.Session.get('member');
     Return Member.Name;
     }
}
```
- - - 
##Access Platform cache in Visualforce page

Access cached values stored in the platform cache from a Visualforce page by using the $Cache.Session or $Cache.Org global variables. By using these global variables, you can read cached values that were stored with Apex directly from your Visualforce page.

- When using the $Cache.Session global variable, fully qualify the key name with the namespace and partition name. This example is an output text component that retrieves a cached value from the namespace 'MyPackagePrefix’, partition PlatformCache, and key myName.

```<apex:outputText value="{!$Cache.Session.MyPackagePrefix.PlatformCache.myName }"/>```

- Unlike with Apex methods, you can’t omit the namespace.partition prefix to reference the default partition in the org. If a namespace isn’t defined for the org, use local to refer to the namespace of the current org where the code is running.

```<apex:outputText value="{!$Cache.Session.local.PlatformCache.myName }"/>```

- If the cached value is a data structure that has properties or methods, like an Apex List or a custom class, access those properties in the $Cache.Session expression using dot notation. For example, this markup invokes the List.size() Apex method if the value of numbersList is declared as a List.

```<apex:outputText value="{!$Cache.Session.local.MyPartition.numbersList.size}"/>```

- This example accesses the value property on the myData cache value that is declared as a custom class.

```<apex:outputText value="{!$Cache.Session.local.MyPartition.myData.value}"/>```






